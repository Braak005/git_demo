function days = countdays(startyear,endyear)
% counts the number of days between two years

days = countdays1(endyear) - countdays1(startyear);

function days = countdays1(year)
% counts the number of days from Jan 1st 1 AD and Jan 1st of input year

days = (year-1)*365 + CountLeaps(year);