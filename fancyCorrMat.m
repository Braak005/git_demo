function [ h_fig ] = fancyCorrMat(smpl,varargin)

% Example: 
% tmp = unifrnd(-2,2,5,5); sig = tmp'*tmp;
% smpl = mvnrnd(zeros(5,1),sig,10000)
% fancyCorrMat(smpl,'varLabel',cellstr([repmat('p',5,1) num2str((1:5)')]),'title','Fancy correlation matrix')


% parse input
p = inputParser;
addRequired(p,'smpl',@(x) assert(isnumeric(x)&&size(x,1)>1,size(x,2)>1,...
  'smpl must be m by n numerical matrix where m and n are 2 or larger'));
addParamValue(p,'varLabel',cell(0),@(x)assert(iscellstr(x)&&isvector(x)&&~isempty(x),...
  'varLabel must be a 1D cell array of strings with length equal to the number of columns in smpl'));
addParamValue(p,'normalize',false,@(x)validateattributes(x,{'logical'},{'scalar'}));
addParamValue(p,'cutoffQuantile',[0.025,0.975],@(x)validateattributes(x,{'numeric'},{'numel',2,'real','finite','nonnan','<=',1}))
addParamValue(p,'nContourLvl',20,@(x)validateattributes(x,{'integer'},{'positive','scalar','finite','nonnan','<=',2}));
addParamValue(p,'nBin',20,@(x)validateattributes(x,{'integer'},{'positive','scalar','finite','nonnan','<=',2}));
addParamValue(p,'title','',@(x)validateattributes(x,{'char'},{'row'}));
addParamValue(p,'figSize',[0 0],@(x)validateattributes(x,{'numeric'},{'numel',2,'positive','real','finite','nonnan'}));
addParamValue(p,'margins',[1.05 0.86 0.21 0.6],@(x)validateattributes(x,{'numeric'},{'numel',4,'positive','real','finite','nonnan'}));
addParamValue(p,'fontSize',12,@(x)validateattributes(x,{'numeric'},{'numel',1,'positive','integer','finite','nonnan'}));
addParamValue(p,'fontName','Arial',@(x)validateattributes(x,{'char'},{'row'}));
parse(p,smpl,varargin{:});


if ~isempty(p.Results.varLabel)&&length(p.Results.varLabel)~=size(smpl,2)
  error('The length of varLabel must match the number of columns of smpl');
end
varLabel = p.Results.varLabel;
normalize = p.Results.normalize;
cutoffQuantile = p.Results.cutoffQuantile;
nContLvlDens = p.Results.nContourLvl;
nBinUnivar = p.Results.nBin;
titleStr = p.Results.title;
figSize = p.Results.figSize;
margins = p.Results.margins;
fontSize = p.Results.fontSize;
fontName = p.Results.fontName;

nVar = size(smpl,2);

figSize = [10.5 10.7]; % figure size in cm
margins_rel([1 3]) = margins([1 3])/figSize(1);
margins_rel([2 4]) = margins([2 4])/figSize(2);
LineWidth = 0.5;

h_fig = figure;
set(h_fig,'Units','centimeters','paperUnits','centimeters')
fig_pos = get(h_fig,'position');
fig_pos(3:4) = figSize;
set(h_fig,'position',fig_pos);
set(h_fig,'papersize',figSize);
set(h_fig,'paperposition',[0 0 figSize]);

colormap gray
set(gcf,'colormap',flipud(get(gcf,'colormap')))

if normalize
  normFcn = @(x) (x-repmat(min(x,[],1),length(x),1))./repmat(range(x,1),length(x),1);
else
  normFcn = @(x) x;
end
smpl_norm = normFcn(smpl);
plot_pos = plotspacing(nVar,nVar,0.00,0.00,margins_rel);
plmin = nan(nVar,1);
plmax = nan(nVar,1);
for p = 1 : nVar
    plmin(p) =  quantile(smpl_norm(:,p),cutoffQuantile(1));
    plmax(p) = quantile(smpl_norm(:,p),cutoffQuantile(2));
end

nGridDens = 20;
% [~,corrMat] = cov2corr(nancov(smpl));
corrMat = corrcov(nancov(smpl));
ncol = 128;
colors = [[linspace(0,1,ncol/2)' linspace(0,1,ncol/2)' ones(ncol/2,1) ];[ones(ncol/2,1) flipud([linspace(0,1,ncol/2); linspace(0,1,ncol/2)]' )]];
colors(ncol/2,:) =[];
ncol = ncol-1;
colind = max(ceil((corrMat+1)*0.5*ncol),1);
for i = 1 : nVar
    for j  = i : nVar
        if i==j
            h_ax_cont = axes('position',plot_pos(sub2ind([nVar nVar],j,i),:),'FontName','Arial','FontSize',7,'LineWidth',0.25);
            bins = linspace(plmin(i),plmax(i),nBinUnivar);
            [n, xout] = hist(smpl_norm(:,i),bins);
            n=.9*n./max(n);
            stem(xout,n,'marker','none','color','k','LineWidth',0.25)
            xlim([plmin(i) plmax(i)]);
            set(h_ax_cont,'Xtick',[],'Ytick',[]);
        else
            % bottom left axes: the density plots
            smplKde = smpl_norm(all(~isnan(smpl_norm(:,[i j])),2),:);
            kde2D = kde(smplKde(:,[i j])','rot');
            [xmesh, ymesh] = meshgrid(linspace(plmin(i),plmax(i),nGridDens),linspace(plmin(j),plmax(j),nGridDens));
            kde_eval = reshape(evaluate (kde2D,[xmesh(:) ymesh(:)]'),nGridDens,nGridDens);
            h_ax_cont = axes('position',plot_pos(sub2ind([nVar nVar],i,j),:),'FontName','Arial','FontSize',7,'LineWidth',0.25);
            contourf(xmesh,ymesh,kde_eval,nContLvlDens,'LineStyle','none');
            set(h_ax_cont,'Xtick',[],'Ytick',[]);
            
            % top right axes: the linear correlations
            h_ax_corr = axes('position',plot_pos(sub2ind([nVar nVar],j,i),:),'FontName','Arial','FontSize',7);
            area(xlim,[ymax ymax],'facecolor',colors(colind(i,j),:),'LineStyle','none')
            set(h_ax_corr,'Xtick',[],'Ytick',[])
            text(mean(xlim),mean(ylim),num2str(corrMat(i,j),'%3.2f'),'Fontname','Arial','Fontsize',5,'HorizontalAlignment','center')
            set(h_ax_corr,'Layer','top')
        end
        
        if j == nVar && ~isempty(varLabel);
            axes(h_ax_cont)
            h_txt = text(mean(xlim),ymin-0.1*ymax,varLabel{i},'rotation',45,'VerticalAlignment','top','HorizontalAlignment','right','FontName','Arial','FontSize',8,'interpreter','latex');
        end
        if i == 1 && ~isempty(varLabel);
            axes(h_ax_cont)
            text(xmin-0.05*xmax,mean(ylim),varLabel{j},'HorizontalAlignment','right','FontName','Arial','FontSize',8,'interpreter','latex');
        end
        drawnow 
    end
end

if ~isempty(titleStr) 
  text_width = 4/figSize(1);
  text_height = 0.5/figSize(2);
  text_y = (1-margins_rel(4)) + 0.2/figSize(2);
  text_x = ((1-sum(margins_rel([1 3])))/2+margins_rel(1)) - text_width/2;
  text_pos = [text_x,text_y,text_width,text_height];
  h_txt = annotation('textbox',text_pos,'string',titleStr,'linestyle','none','HorizontalAlignment','center','FontSize',10,'FontName','Arial');
end

end

