function no = dayno(month,day,leap)
% This function converts a month number and day of the month into an annual
% daynumber, while considering possible leap days

firstdaymonth = [0 31 59 90 120 151 181 212 243 273 304 334];

no = firstdaymonth(month) + day + (leap && month>2);