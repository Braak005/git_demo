function leaps = countleaps(year)
% calculates number of leapyears since 1 AD
leaps = floor((year-1)/4) - floor((year-1)/100) + floor((year-1)/400) + 1;
